package com.example.redispractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataRedisPracticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDataRedisPracticeApplication.class, args);
    }

}
